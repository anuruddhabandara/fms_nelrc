import React, { Component } from 'react';
//import axios from 'axios';
import axios, { post } from 'axios';
class Search extends Component{
 constructor(props) {
    super(props);
    this.state ={
      file:null
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file).then((response)=>{
      console.log(response.data);
    })
  }
  onChange(e) {
    this.setState({file:e.target.files[0]})
  }
  fileUpload(file){
    const url = 'http://localhost:40/Uploads/upload.php';
    const fileToUpload = new FormData();
    fileToUpload.append('file',file)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return  post(url, fileToUpload,config)
  }

	
	render(){
		
	
	  return(
			<div class="container">
				 <form onSubmit={this.onFormSubmit}>
        <input type="file" onChange={this.onChange} />
        <button type="submit">Upload</button>
				</form>
				</div>
            )	
		
		
	}
}
export default Search;