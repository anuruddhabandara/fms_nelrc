import React, { Component } from 'react';
import axios from 'axios';
class ResultList extends Component{
   constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
  }
  componentDidMount()
  {
    axios.get("http://localhost:40/Uploads/FileList.php")
      .then(res => {
        const posts = res.data;
        this.setState({ posts });
      });
  }
  Filedelete(value)
  {
   axios.post("http://localhost:40/Uploads/DeleteFile.php", {value})
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render() {
    return (	  
	    <table class="table table-condensed">           
          <tr>
            <th>File Name</th>
            <th>Describtion</th>
            <th>Location</th>
			<th>Action</th>			
          </tr>       
       <tbody>
              {this.state.posts.map(result =>
                <tr key={result.name}>
                  <td>{result.name}</td>
                  <td>{result.discription}</td>
                  <td>{result.location}</td>
				  <td> <input type="button" value = "Delete" onClick={this.Filedelete.bind(this,result.name)}/></td>
				   
                </tr>
              )}
        </tbody>
     </table>  
	  
    );
  }
}
export default ResultList;